<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sales
 *
 * @ORM\Table(name="sales", indexes={@ORM\Index(name="fk_viajero", columns={"fk_viajero"}), @ORM\Index(name="fk_viaje", columns={"fk_viaje"})})
 * @ORM\Entity
 */
class Sales
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Travelers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Travelers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_viajero", referencedColumnName="id")
     * })
     */
    private $fkViajero;

    /**
     * @var \AppBundle\Entity\Trips
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Trips")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_viaje", referencedColumnName="id")
     * })
     */
    private $fkViaje;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @return \Int  
     */
    public function getFkViajero() 
    {
        return $this -> fkViajero;
    }

    /**
     * @param \Int $fkViajero 
     */
    public function setFkViajero( $fkViajero ) 
    {
        $this -> fkViajero = $fkViajero;
    }

    /**
     * @return \Int  
     */
    public function getFkViaje() 
    {
        return $this -> fkViaje;
    }

    /**
     * @param \Int $fkViaje 
     */
    public function setFkViaje( $fkViaje ) 
    {
        $this -> fkViaje = $fkViaje;
    }

    /**
     * @return \Datetime 
     */
    public function getCreatedAt() 
    {
        return $this -> createdAt;
    }

    /**
     * @param \Datetime $createdAt 
     */
    public function setCreatedAt( $createdAt = null ) 
    {
        $this -> createdAt = new \DateTime( 'now', new \DateTimeZone( 'America/Caracas' ) );
    }

    /**
     * @return \Datetime 
     */
    public function getUpdatedAt() 
    {
        return $this -> updatedAt;
    }

    /**
     * @param \Datetime $createdAt 
     */
    public function setUpdatedAt( $updatedAt = null ) 
    {
        $this -> updatedAt = new \DateTime( 'now', new \DateTimeZone( 'America/Caracas' ) );
    }
}

