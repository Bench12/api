<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trips
 *
 * @ORM\Table(name="trips", uniqueConstraints={@ORM\UniqueConstraint(name="codigo_2", columns={"codigo"})}, indexes={@ORM\Index(name="codigo", columns={"codigo"})})
 * @ORM\Entity
 */
class Trips
{
    /**
    * @var integer
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="IDENTITY")
    */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="plazas", type="integer", nullable=false)
     */
    private $plazas;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=false)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=20, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=255, nullable=false)
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="destino", type="string", length=255, nullable=false)
     */
    private $destino;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @return \Int 
     */
    public function getId() 
    {
        return $this -> id;
    }

    /**
     * @return \Int 
     */
    public function getPlazas() 
    {
        return $this -> plazas;
    }

    /**
     * @param \Int $plazas 
     */
    public function setPlazas( $plazas ) 
    {
        $this -> plazas = $plazas;
    }

    /**
     * @param \Float $precio 
     */
    public function setPrecio( $precio ) 
    {
        $this -> precio = $precio;
    }

    /**
     * @return \Float 
     */
    public function getPrecio() 
    {
        return $this -> precio;
    }

    /**
     * @param \String $codigo 
     */
    public function setCodigo( $codigo ) 
    {
        $this -> codigo = $codigo;
    }

    /**
     * @return \String
     */
    public function getCodigo() 
    {
        return $this -> codigo;
    }

    /**
    * @param \String $origen 
    */
    public function setOrigen( $origen ) 
    {
        $this -> origen = $origen;
    }

    /**
     * @return \String 
     */
    public function getOrigen() 
    {
        return $this -> origen;
    }

    /**
    * @param \String $destino 
    */
    public function setDestino( $destino ) 
    {
        $this -> destino = $destino;
    }

    /**
     * @return \String 
     */
    public function getDestino() 
    {
        return $this -> destino;
    }

    /**
     * @return \Datetime 
     */
    public function getCreatedAt() 
    {
        return $this -> createdAt;
    }

    /**
     * @param \Datetime $createdAt 
     */
    public function setCreatedAt( $createdAt = null ) 
    {
        $this -> createdAt = new \DateTime( 'now', new \DateTimeZone( 'America/Caracas' ) );
    }

    /**
     * @return \Datetime 
     */
    public function getUpdatedAt() 
    {
        return $this -> updatedAt;
    }

    /**
     * @param \Datetime $createdAt 
     */
    public function setUpdatedAt( $updatedAt = null ) 
    {
        $this -> updatedAt = new \DateTime( 'now', new \DateTimeZone( 'America/Caracas' ) );
    }
}

