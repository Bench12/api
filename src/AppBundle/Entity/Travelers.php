<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use DateTime;
use DateTimeZone;

/**
 * Travelers
 *
 * @ORM\Table(name="travelers", uniqueConstraints={@ORM\UniqueConstraint(name="cedula_2", columns={"cedula"})}, indexes={@ORM\Index(name="cedula", columns={"cedula"})})
 * @ORM\Entity
 */
class Travelers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cedula", type="string", length=20, nullable=false)
     */
    private $cedula;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="text", length=65535, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=10, nullable=true)
     */
    private $telefono;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @return \int 
     */
    public function getId() 
    {
        return $this -> id;
    }

    /**
     * @return \String 
     */
    public function getCedula() 
    {
        return $this -> cedula;
    }

    /**
     * @param \String $cedula 
     */
    public function setCedula( $cedula ) 
    {
        $this -> cedula = $cedula;
    }

    /**
     * @return \String 
     */
    public function getNombre() 
    {
        return $this -> nombre;
    }

    /**
     * @param \String $nombre 
     */
    public function setNombre( $nombre ) 
    {
        $this -> nombre = $nombre;
    }

    /**
     * @return \String 
     */
    public function getDireccion() 
    {
        return $this -> direccion;
    }

    /**
     * @param \String $direccion 
     */
    public function setDireccion( $direccion ) 
    {
        $this -> direccion = $direccion;
    }

    /**
     * @return \String 
     */
    public function getTelefono() 
    {
        return $this -> telefono;
    }

    /**
     * @param \String $telefono 
     */
    public function setTelefono( $telefono ) 
    {
        $this -> telefono = $telefono;
    }

    /**
     * @return \Datetime 
     */
    public function getCreatedAt() 
    {
        return $this -> createdAt;
    }

    /**
     * @param \Datetime $createdAt 
     */
    public function setCreatedAt( $createdAt = null ) 
    {
        $this -> createdAt = new \DateTime( 'now', new \DateTimeZone( 'America/Caracas' ) );
    }

    /**
     * @return \Datetime 
     */
    public function getUpdatedAt() 
    {
        return $this -> updatedAt;
    }

    /**
     * @param \Datetime $createdAt 
     */
    public function setUpdatedAt( $updatedAt = null ) 
    {
        $this -> updatedAt = new \DateTime( 'now', new \DateTimeZone( 'America/Caracas' ) );
    }
}

