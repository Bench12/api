<?php 

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Travelers;

/**
* @Route("/traveler")
*/
class TravelerController extends Controller
{
	/**
	*@Route("/")
	*/
	public function actionIndex() 
	{
		$travelers = $this -> getDoctrine() 
			-> getRepository( 'AppBundle:Travelers' ) 
			-> findAll();

		$travelers = $this -> get( 'jms_serializer' ) -> serialize( $travelers, 'json' );

		return new Response( $travelers ); 
	}

	/**
	* @Route("/{id}")
	* @Method("GET")	
	*/
	public function getAction( Travelers $id ) 
	{
		$traveler = $this -> get( 'jms_serializer' ) 
			-> serialize( $id, 'json' );

		return new Response( $traveler );
	}
} 