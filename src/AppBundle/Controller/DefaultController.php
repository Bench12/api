<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use AppBundle\Entity\Travelers;
use AppBundle\Form\TravelersType;

use AppBundle\Entity\Trips;
use AppBundle\Form\TripsType;

use AppBundle\Entity\Sales;
use AppBundle\Form\SalesType;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return new JsonResponse( [ 'status' => 'OK' ] );
    }

    /**
     * @Route("/saveTraveler", name="save_traveler")
     * @Method("POST")
     */
    public function saveTravelerAction( Request $request )
    {
        $data = $request -> getContent();

        parse_str( $data, $data_arr );

        $traveler = new Travelers();

        $form = $this -> createForm( TravelersType :: class, $traveler );

        $form -> submit( $data_arr );

        $doctrine = $this -> getDoctrine() -> getManager();

        $doctrine -> persist( $traveler );

        $doctrine -> flush();

        return new JsonResponse( [ 'msg' => 'New traveler successfully created!' ] );
    }

    /**
     * @Route("/saveTrip", name="save_trip")
     * @Method("POST")
     */
    public function saveTripAction( Request $request )
    {
        $data = $request -> getContent();

        parse_str( $data, $data_arr );

        $trip = new Trips();

        $form = $this -> createForm( TripsType :: class, $trip );

        $form -> submit( $data_arr );

        $doctrine = $this -> getDoctrine() -> getManager();

        $doctrine -> persist( $trip );

        $doctrine -> flush();

        return new JsonResponse( [ 'msg' => 'New trip successfully created!' ] );
    }

    /**
    * @Route("/getTraveler/{cedula}")
    * @Method("GET")    
    */
    public function getTravelerAction( Request $request ) 
    {
        $doctrine = $this -> getDoctrine() -> getManager();

        $traveler = $doctrine -> getRepository( Travelers :: class ) 
            -> findOneBy( [ 'cedula' => $request -> get( 'cedula' ) ] );

        if ( is_null( $traveler ) ) 
            return new JsonResponse( [ 'msg' => 'The traveler doesnt exist.' ] );

        $jsonContent = $this -> get( 'jms_serializer' ) 
            -> serialize( $traveler, 'json' ); 

        return new Response( $jsonContent );
    }

    /**
    * @Route("/getTrip/{codigo}")
    * @Method("GET")    
    */
    public function getTripAction( Request $request ) 
    {
        $doctrine = $this -> getDoctrine() -> getManager();

        $trip = $doctrine -> getRepository( Trips :: class ) 
            -> findOneBy( [ 'codigo' => $request -> get( 'codigo' ) ] );

        if ( is_null( $trip ) ) 
            return new JsonResponse( [ 'msg' => 'The trip code doesnt exist.' ] );

        $jsonContent = $this -> get( 'jms_serializer' ) 
            -> serialize( $trip, 'json' ); 

        return new Response( $jsonContent );
    }

    /**
    * @Route("/getTrips")
    * @Method("GET")    
    */
    public function getTripsAction( Request $request ) 
    {
        $trips = $this -> getDoctrine() 
            -> getRepository( 'AppBundle:Trips' ) 
            -> findAll();

        $jsonContent = $this -> get( 'jms_serializer' ) -> serialize( $trips, 'json' );

        return new Response( $jsonContent ); 
    }


    /**
    * @Route("/createTravel", name="create_travel")
    * @Method("POST")
    */
    public function createTravelAction( Request $request ) 
    {
        $doctrine = $this -> getDoctrine() -> getManager();

        $traveler = $doctrine -> getRepository( Travelers :: class ) 
            -> findOneBy( [ 'cedula' => $request -> get( 'cedula' ) ] );

        if ( is_null( $traveler ) ) 
            return new JsonResponse( [ 'msg' => 'The traveler doesnt exist.' ] );

        $trip = $doctrine -> getRepository( Trips :: class )
            -> findOneBy( [ 'codigo' => $request -> get( 'codigo' ) ] );

        if ( is_null( $traveler ) ) 
            return new JsonResponse( [ 'msg' => 'The trip code doesnt exist.' ] );

        $data_arr = [];

        $data_arr[ 'fkViajero' ] = $traveler -> getId();
        $data_arr[ 'fkViaje' ]   = $trip     -> getId();

        $sales = new Sales();

        $form = $this -> createForm( SalesType :: class, $sales );

        $form -> submit( $data_arr );

        $doctrine -> persist( $sales );

        $doctrine -> flush();

        return new JsonResponse( [ 'msg' => 'The traveler ' . $traveler -> getCedula() 
            . ' ' . 'has been assoccieated with the trip ' . $trip -> getCodigo() ] );
    }
}
